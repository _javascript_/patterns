class Task {
  public type: string;
  public os: string;
  public name: string;
  public description: string;
}

interface TaskBuilder {
  insertType(): void;
  insertOs(): void;
  insertName(): void;
  insertDescription(): void;
}

class DefaultTaskBuilder implements TaskBuilder {
  private task: Task;
  constructor() {
    this.reset();
  }
  public reset(): void {
    this.task = new Task();
  }

  public insertType(): void {
    this.task.type = "Default TYPE";
  }
  public insertOs(): void {
    this.task.os = "Default OS";
  }
  public insertName(): void {
    this.task.name = "Default NAME";
  }
  public insertDescription(): void {
    this.task.description = "Default DESCRIPTION";
  }
  public build(): Task {
    const result = this.task;
    this.reset();
    return result;
  }
}

class TaskDirector {
  private builder: TaskBuilder;

  public setBuilder(builder: TaskBuilder): void {
    this.builder = builder;
  }

  public buildMinimalViableTask(): void {
    this.builder.insertType();
  }

  public buildFullFeaturedTask(): void {
    this.builder.insertName();
    this.builder.insertOs();
    this.builder.insertType();
    this.builder.insertDescription();
  }
}

function clientCode(director: TaskDirector) {
  const builder = new DefaultTaskBuilder();
  director.setBuilder(builder);

  director.buildMinimalViableTask();
  let task = builder.build();
  console.log("Standard basic task:", task);

  director.buildFullFeaturedTask();
  task = builder.build();
  console.log("Standard full featured task:", task);

  builder.insertName();
  builder.insertOs();
  task = builder.build();
  console.log("Custom task:", task);
}

const director = new TaskDirector();
clientCode(director);
