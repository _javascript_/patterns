class Database {
  private static instance: Database;

  private constructor() {}

  public static getInstance(): Database {
    if (!Database.instance) {
      Database.instance = new Database();
    }

    return Database.instance;
  }

  public successMessage() {
    return "Singleton works, both variables contain the same instance.";
  }
}

function clientCode() {
  const s1 = Database.getInstance();
  const s2 = Database.getInstance();

  if (s1 === s2) {
    console.log(s1.successMessage());
  } else {
    console.log("Singleton failed, variables contain different instances.");
  }
}

clientCode();
