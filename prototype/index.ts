class Pizza {
  public chesse: boolean;
  public souce: string;
  public options: object;
  public circularReference: ComponentWithBackReference;

  static default(): Pizza {
    const pizza = new Pizza();
    pizza.chesse = true;
    pizza.souce = "tomato";
    pizza.options = {};
    pizza.circularReference = { pizza };
    return pizza;
  }

  public clone(): this {
    const clone = { ...this };
    clone.options = Object.create(this.options);
    clone.circularReference = { ...this.circularReference, pizza: { ...this } };

    return clone;
  }
}

class ComponentWithBackReference {
  public pizza;

  constructor(pizza: Pizza) {
    this.pizza = pizza;
  }
}

function clientCode() {
  const prototypePizza = Pizza.default();

  const pizza1 = prototypePizza.clone();
  const pizza2 = prototypePizza.clone();

  pizza1.souce = "creamy";
  pizza1.options = { chicken: true };

  console.log({ prototypePizza, pizza1, pizza2 });
}

clientCode();
