abstract class Strategy {
  protected speed: number;
  abstract calculatenDistance(time: number): string;
  message(strategyName: string, time: number, distance: number): string {
    return `[${strategyName}]:\nspeed -> ${this.speed}km/h\ntime -> ${time}h\ndistance -> ${distance}km`;
  }
}

class BikeStrategy extends Strategy {
  constructor() {
    super();
    this.speed = 18;
  }
  public calculatenDistance(time: number): string {
    const distance = this.speed * time;
    return this.message("BikeStrategy", time, distance);
  }
}

class BusStrategy extends Strategy {
  constructor() {
    super();
    this.speed = 40;
  }
  public calculatenDistance(time: number): string {
    const distance = this.speed * time;
    return this.message("BusStrategy", time, distance);
  }
}

class CarStrategy extends Strategy {
  constructor() {
    super();
    this.speed = 60;
  }
  public calculatenDistance(time: number): string {
    const distance = this.speed * time;
    return this.message("CarStrategy", time, distance);
  }
}

class Context {
  private strategy: Strategy;
  constructor(strategy: Strategy) {
    this.strategy = strategy;
  }

  public setStrategy(strategy: Strategy) {
    this.strategy = strategy;
  }

  public execute(): void {
    const times = [1, 2, 5];
    for (const time of times) {
      const result = this.strategy.calculatenDistance(time);
      console.log(result);
    }
  }
}

const context = new Context(new BikeStrategy());
console.log("Client: Strategy BikeStrategy.");
context.execute();

console.log("");

console.log("Client: Strategy BusStrategy.");
context.setStrategy(new BusStrategy());
context.execute();

console.log("");

console.log("Client: Strategy CarStrategy.");
context.setStrategy(new CarStrategy());
context.execute();
