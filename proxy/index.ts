interface Subject {
  request(): void;
}

class RealSubject implements Subject {
  public request(): void {
    console.log("[RealSubject] -> request");
  }
}

class ProxySubject implements Subject {
  private realSubject: RealSubject;
  constructor(realSubject: RealSubject) {
    this.realSubject = realSubject;
  }
  public request(): void {
    if (this.checkAccess()) {
      this.realSubject.request();
      this.logAccess();
    }
  }

  private checkAccess(): boolean {
    console.log("[ProxySubject] -> checkAccess");

    return true;
  }

  private logAccess(): void {
    console.log("[ProxySubject] -> logAccess");
  }
}

function clientCode(subject: Subject) {
  subject.request();
}

const realSubject = new RealSubject();
clientCode(realSubject);
console.log("============");
const proxy = new ProxySubject(realSubject);
clientCode(proxy);
