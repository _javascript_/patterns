interface INotifier {
  send(message: string): void;
}

class Notifier implements INotifier {
  send(message: string): void {
    console.log(`[Notifier] -> ${message}`);
  }
}

class Decorator implements INotifier {
  protected notifier: INotifier;
  constructor(notifier: INotifier) {
    this.notifier = notifier;
  }
  send(message: string): void {
    this.notifier.send(message);
  }
}

class SMSDecorator extends Decorator {
  send(message: string) {
    super.send("[SMSDecorator] -> " + message);
  }
}

class FacebookDecorator extends Decorator {
  send(message: string) {
    super.send("[FacebookDecorator] -> " + message);
  }
}

function clientCode(notifier: INotifier) {
  notifier.send("Client Code");
}

const notifier = new Notifier();
clientCode(notifier);

const smsNotifier = new SMSDecorator(notifier);
clientCode(smsNotifier);

const facebookNotifier = new FacebookDecorator(notifier);
clientCode(facebookNotifier);

const smsAndFacebookNotifier = new FacebookDecorator(smsNotifier);
clientCode(smsAndFacebookNotifier);
