interface Info {
  value: string;
}

class Api {
  public getInfo(): Info {
    const info = { value: "default value" };
    console.log("[Api]: getInfo -> ", info);
    return info;
  }
}

class ExternalApi {
  public getData(): string {
    const data = "default data";
    console.log("[ExternalApi]: getData -> ", data);
    return data;
  }
}

class ExternalApiAdapter extends Api {
  private adaptee: ExternalApi;
  constructor(adaptee: ExternalApi) {
    super();
    this.adaptee = adaptee;
  }

  public getInfo(): Info {
    const result = this.adaptee.getData();
    const info = { value: result };
    console.log("[ExternalApiAdapter]: getInfo -> ", info);
    return info;
  }
}

function clientCode(api: Api) {
  api.getInfo();
}

console.log("Взаимодействие с стондартным апи");
const defaultApi = new Api();
clientCode(defaultApi);

console.log(" ");

console.log("Взаимодействие с стороним апи");
const externalApi = new ExternalApi();
const externalApiAdapter = new ExternalApiAdapter(externalApi);
clientCode(externalApiAdapter);
