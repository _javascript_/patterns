enum OSType {
  MAC = "MAC",
  WIN = "WIN",
}

// Aбстрактный продукт Task

abstract class Task {
  readonly os: OSType;

  public abstract getDescription(): string;

  public getDefaultDescription(): string {
    return `[${this.os}Task] DEFAULT description`;
  }
  public getTaskInfo(): string {
    return `[Task]: getTaskInfo() -> os = ${this.os}`;
  }
}

// Конкретный продукт

class WinTask extends Task {
  os = OSType.WIN;

  public getDescription(): string {
    return this.getDefaultDescription();
  }
}

class MacTask extends Task {
  os = OSType.MAC;
  public getDescription(): string {
    return `[MacTask] NOT DEFAULT description`;
  }
}

// Aбстрактный продукт Event
abstract class MyEvent {
  readonly os: OSType;

  public abstract getEventInfo(): string;
  public getTaskInfo(task: Task): string {
    return task.getTaskInfo();
  }
}

class WinEvent extends MyEvent {
  os = OSType.WIN;
  public getEventInfo(): string {
    return `[${this.os}Event]: event info`;
  }
}

class MacEvent extends MyEvent {
  os = OSType.MAC;
  public getEventInfo(): string {
    return `[${this.os}Event]: event info`;
  }
}

// Создаём обстрактную фабрику которая реализует методы по
// созданию таска и евента с абстрактным возвращаемым типом

interface OsFactory {
  createTask(): Task;
  createEvent(): MyEvent;
}

// Создаём конкретную фабрику которая реализует методы по
// созданию конкретного таска и евента НО! с абстрактным возвращаемым типом

class WinFactory implements OsFactory {
  createTask(): Task {
    return new WinTask();
  }
  createEvent(): MyEvent {
    return new WinEvent();
  }
}

class MacFactory implements OsFactory {
  createTask(): Task {
    return new MacTask();
  }
  createEvent(): MyEvent {
    return new MacEvent();
  }
}

function clientCode(factory: OsFactory) {
  const task = factory.createTask();
  const event = factory.createEvent();

  const info = event.getTaskInfo(task);
  console.log({ task, event, info });
}

// const os = require("os");

const os = {
  type: "Win",
};

if (os.type.toLowerCase() === OSType.WIN.toLocaleLowerCase()) {
  console.log("Client: Testing client code with the first factory type...");
  clientCode(new WinFactory());
} else if (os.type.toLowerCase() === OSType.MAC.toLocaleLowerCase()) {
  console.log(
    "Client: Testing the same client code with the second factory type..."
  );
  clientCode(new MacFactory());
} else {
  console.log("Error");
}
