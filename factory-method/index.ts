// Создать интерфейс "Продукта"

interface Task {
  getDescription(): string;
}

// Создать "Конкретный" "Продукт"

class MakeBreakfast implements Task {
  public getDescription(): string {
    return "[Task] Make a Breakfast";
  }
}

class WashFloors implements Task {
  public getDescription(): string {
    return "[Task] Wash the Floors";
  }
}

// Создать абстрактный "Creator"

abstract class TaskCreator {
  public abstract create(): Task;
  public someOperation(): string {
    const task = this.create();
    return `[Creator] someOperation -> task.getDescription() = ${task.getDescription()}`;
  }
}

// Создать "Конкретный" "Creator"

class TaskCreatorMakeBreakfast extends TaskCreator {
  public create(): Task {
    return new MakeBreakfast();
  }
}

class TaskCreatorWashFloors extends TaskCreator {
  public create(): Task {
    return new WashFloors();
  }
}

function clientCode(creator: TaskCreator) {
  const task = creator.create();
  console.log("[clientCode]: task.getDescription() ", task.getDescription());
  console.log(
    "[clientCode]: creator.someOperation() ",
    creator.someOperation()
  );
}

console.log("App: TaskCreatorMakeBreakfast");
clientCode(new TaskCreatorMakeBreakfast());
console.log("");

console.log("App: TaskCreatorWashFloors");
clientCode(new TaskCreatorWashFloors());
