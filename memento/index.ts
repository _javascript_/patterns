const TOKEN_LENGTH = 32;
const ALPHA_UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const ALPHA_LOWER = "abcdefghijklmnopqrstuvwxyz";
const ALPHA = ALPHA_UPPER + ALPHA_LOWER;
const DIGIT = "0123456789";
const ALPHA_DIGIT = ALPHA + DIGIT;

const generateToken = () => {
  const base = ALPHA_DIGIT.length;
  let key = "";
  for (let i = 0; i < TOKEN_LENGTH; i++) {
    const index = Math.floor(Math.random() * base);
    key += ALPHA_DIGIT[index];
  }
  return key;
};

interface MyState {
  number: number;
  string: string;
}

class MySuperObject {
  private state: MyState;

  constructor(state: MyState) {
    this.state = state;
    console.log(`Originator: My initial state is: `, state);
  }

  public increment(): void {
    console.log(`[increment] Old state: `, this.state);
    this.state.number = this.state.number + 1;
    console.log(`[increment] New state: `, this.state);
  }

  public insertString(): void {
    console.log(`[insertString] Old state: `, this.state);
    this.state.string = generateToken();
    console.log(`[insertString] New state: `, this.state);
  }

  public save(): Memento {
    return new ConcreteMemento(this.clone().state);
  }

  public restore(memento: Memento): void {
    this.state = memento.getState();
    console.log(`Originator: My state has changed to: `, this.state);
  }

  public clone(): MySuperObject {
    const state = { ...this.state };
    const obj = new MySuperObject(state);
    return obj;
  }
}

interface Memento {
  getState(): MyState;
  getName(): string;
  getDate(): string;
}

class ConcreteMemento implements Memento {
  private state: MyState;

  private date: string;

  constructor(state: MyState) {
    this.state = state;
    this.date = new Date().toISOString().slice(0, 19).replace("T", " ");
  }

  public getState(): MyState {
    return this.state;
  }

  public getName(): string {
    return `${this.date} / (${this.state.string.substr(0, 9)}...)`;
  }

  public getDate(): string {
    return this.date;
  }
}

class Caretaker {
  private mementos: Memento[] = [];
  private originator: MySuperObject;

  constructor(originator: MySuperObject) {
    this.originator = originator;
  }

  public backup(): void {
    console.log("\nCaretaker: Saving Originator's state...");
    this.mementos.push(this.originator.save());
  }

  public undo(): void {
    if (!this.mementos.length) {
      return;
    }
    const memento = this.mementos.pop();

    console.log(`Caretaker: Restoring state to: ${memento.getName()}`);
    this.originator.restore(memento);
  }

  public showHistory(): void {
    console.log("Caretaker: Here's the list of mementos:");
    console.log(this.mementos);
    for (const memento of this.mementos) {
      console.log(memento.getName());
    }
  }
}

const myObject = new MySuperObject({ number: 0, string: "" });
const caretaker = new Caretaker(myObject);

caretaker.backup();

myObject.increment();
myObject.insertString();

caretaker.backup();

myObject.increment();
myObject.insertString();

caretaker.backup();

myObject.increment();
myObject.insertString();

console.log("");
caretaker.showHistory();

console.log("\nClient: Now, let's rollback!\n");
caretaker.undo();

console.log("\nClient: Once more!\n");
caretaker.undo();
