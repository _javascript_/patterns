type Percent = number;
type Channel = number;

abstract class Device {
  protected enabled: boolean;
  protected percent: Percent;
  protected channel: Channel;
  isEnabled(): boolean {
    return this.enabled;
  }
  enable(): void {
    this.enabled = true;
  }
  disable(): void {
    this.enabled = false;
  }
  log(action?: string): void {
    const { enabled, percent, channel } = this;
    action = action ? `/${action}` : "";
    console.log(`[Device${action}]: log -> `, { enabled, percent, channel });
  }
  abstract getVolume(): Percent;
  abstract setVolume(percent: Percent): void;
  abstract getChannel(): Channel;
  abstract setChannel(channel: Channel): void;
}

class Remote {
  protected device: Device;
  constructor(device: Device) {
    this.device = device;
  }

  togglePower() {
    const { device } = this;
    if (device.isEnabled()) device.disable();
    else device.enable();
    device.log("togglePower");
  }
  volumeDown() {
    const { device } = this;
    const volume = device.getVolume();
    device.setVolume(volume - 1);
    device.log("volumeDown");
  }
  volumeUp() {
    const { device } = this;
    const volume = device.getVolume();
    device.setVolume(volume + 1);
    device.log("volumeUp");
  }
  channelDown() {
    const { device } = this;
    const channel = device.getChannel();
    device.setChannel(channel - 1);
    device.log("channelDown");
  }
  channelUp() {
    const { device } = this;
    const channel = device.getChannel();
    device.setChannel(channel + 1);
    device.log("channelUp");
  }
}

class AdvancedRemote extends Remote {
  private isMute: boolean;
  private oldVolume: Percent;
  constructor(device: Device) {
    super(device);
    this.isMute = false;
    this.oldVolume = this.device.getVolume();
  }
  mute() {
    if (this.isMute) {
      this.device.setVolume(this.oldVolume);
      this.isMute = false;
    } else {
      this.oldVolume = this.device.getVolume();
      this.device.setVolume(0);
      this.isMute = true;
    }
    this.device.log("mute");
  }
}

class Radio extends Device {
  constructor() {
    super();
    this.percent = 10;
    this.channel = 1;
    this.enabled = false;
  }
  getVolume(): Percent {
    return this.percent;
  }
  setVolume(percent: Percent): void {
    this.percent = percent;
  }
  getChannel(): Channel {
    return this.channel;
  }
  setChannel(channel: Channel): void {
    this.channel = channel;
  }
}

class TV extends Device {
  constructor() {
    super();
    this.percent = 5;
    this.channel = 1;
    this.enabled = false;
  }
  getVolume(): Percent {
    return this.percent;
  }
  setVolume(percent: Percent): void {
    if (this.percent < 0) return;
    this.percent = percent;
  }
  getChannel(): Channel {
    return this.channel;
  }
  setChannel(channel: Channel): void {
    if (this.channel < 1) channel = 10;
    if (this.channel > 10) channel = 1;
    this.channel = channel;
  }
}

function clientCode(remote: Remote | AdvancedRemote) {
  remote.togglePower();
  remote.volumeUp();
  remote.volumeUp();
  remote.volumeDown();
  remote.channelDown();
  remote.channelDown();
  remote.channelUp();
  remote.channelUp();
  remote.channelUp();
  (remote as AdvancedRemote).mute && (remote as AdvancedRemote).mute();
  (remote as AdvancedRemote).mute && (remote as AdvancedRemote).mute();
  remote.togglePower();
}

const tv = new TV();
const remoteTv = new AdvancedRemote(tv);
clientCode(remoteTv);

// const radio = new Radio();
// const remoteRadio = new Remote(radio);
// clientCode(remoteRadio);
