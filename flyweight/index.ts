class Flyweight {
  private sharedState: any;

  constructor(sharedState: any) {
    this.sharedState = sharedState;
  }

  public operation(uniqueState: any): void {
    const s = JSON.stringify(this.sharedState);
    const u = JSON.stringify(uniqueState);

    console.log(`[Flyweight]:\n shared state (${s})\n unique state (${u}).`);
  }
}

class FlyweightFactory {
  private flyweights: {
    [key: string]: Flyweight;
  } = {};

  constructor(initialFlyweights: string[][]) {
    for (const state of initialFlyweights) {
      const key = this.getKey(state);
      this.flyweights[key] = new Flyweight(state);
    }
  }

  // Создаём ключ уникальный ключ для добавления его в стейт
  private getKey(state: string[]): string {
    return state.join("_");
  }

  public getFlyweight(sharedState: string[]): Flyweight {
    const key = this.getKey(sharedState);
    if (!this.flyweights.hasOwnProperty(key)) {
      console.log(
        `[FlyweightFactory]: Not found Flyweight by key: ${key} -> create new Flyweight and add in store`
      );
      this.flyweights[key] = new Flyweight(sharedState);
    } else {
      console.log(`[FlyweightFactory]: Found Flyweight by key ${key}`);
    }
    return this.flyweights[key];
  }

  public listFlyweights(): void {
    const count = Object.keys(this.flyweights).length;
    console.log(`\n[FlyweightFactory]:  flyweights lenght = ${count}`);
    for (const key in this.flyweights) {
      console.log(key);
    }
  }
}

const factory = new FlyweightFactory([
  ["Chevrolet", "Camaro2018", "pink"],
  ["Mercedes Benz", "C300", "black"],
  ["Mercedes Benz", "C500", "red"],
  ["BMW", "M5", "red"],
  ["BMW", "X6", "white"],
]);
factory.listFlyweights();

function addCarToPoliceDatabase(
  ff: FlyweightFactory,
  plates: string,
  owner: string,
  brand: string,
  model: string,
  color: string
) {
  console.log("\nClient: Adding a car to database.");
  const flyweight = ff.getFlyweight([brand, model, color]);

  //   В качестве аргумента мы передаём уникальный стэйт
  flyweight.operation([plates, owner]);
}

addCarToPoliceDatabase(factory, "CL234IR", "James Doe", "BMW", "M5", "red");

addCarToPoliceDatabase(factory, "CL234IR", "James Doe", "BMW", "X1", "red");

factory.listFlyweights();
