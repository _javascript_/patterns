function random(min: number, max: number) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

abstract class Component {
  protected parent: Component;

  public setParent(parent: Component) {
    this.parent = parent;
  }

  public getParent(): Component {
    return this.parent;
  }

  public add(component: Component): void {}
  public remove(component: Component): void {}

  public isComposite(): boolean {
    return false;
  }

  public abstract operation(): number;
}

class Product extends Component {
  sum: number;
  constructor() {
    super();
    this.sum = random(1, 100);
  }
  operation() {
    return this.sum;
  }
}

class Box extends Component {
  protected children: Component[] = [];
  public add(component: Component): void {
    this.children.push(component);
    component.setParent(this);
  }
  public remove(component: Component): void {
    const index = this.children.indexOf(component);
    this.children.splice(index, 1);
    component.setParent(null);
  }

  public isComposite(): boolean {
    return true;
  }

  operation() {
    const sum = this.children.reduce((acc, child) => {
      acc += child.operation();
      return acc;
    }, 0);
    return sum;
  }
}

const tree = new Box();

const box1 = new Box();
const box2 = new Box();
const box3 = new Box();

const p1 = new Product();
const p2 = new Product();
const p3 = new Product();
const p4 = new Product();
const p5 = new Product();
const p6 = new Product();

box1.add(p2);
box1.add(p3);

box2.add(p4);
box2.add(p5);

box3.add(p6);

box1.add(box3);

tree.add(p1);
tree.add(box1);
tree.add(box2);

console.log(tree.operation());
