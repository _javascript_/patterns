class Subsystem1 {
  ready(): string {
    return "[Subsystem1] -> ready!\n";
  }

  operationA(): string {
    return "[Subsystem1] -> operationA!\n";
  }
}

class Subsystem2 {
  ready(): string {
    return "[Subsystem2] -> ready!\n";
  }
  operationB(): string {
    return "[Subsystem2] -> operationB!\n";
  }
}

class Facade {
  protected subsystem1: Subsystem1;
  protected subsystem2: Subsystem2;

  constructor(subsystem1: Subsystem1 = null, subsystem2: Subsystem2 = null) {
    this.subsystem1 = subsystem1 || new Subsystem1();
    this.subsystem2 = subsystem2 || new Subsystem2();
  }

  public operation(): string {
    let result = "Facade initializes subsystems:\n";
    result += this.subsystem1.ready();
    result += this.subsystem2.ready();
    result += "Facade orders subsystems to perform the action:\n";
    result += this.subsystem1.operationA();
    result += this.subsystem2.operationB();

    return result;
  }
}

function clientCode(facade: Facade) {
  console.log(facade.operation());
}

const subsystem1 = new Subsystem1();
const subsystem2 = new Subsystem2();
const facade = new Facade(subsystem1, subsystem2);
clientCode(facade);
